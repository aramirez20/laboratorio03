package com.aiperapps.laboratorio3

import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_segunda.*

class SegundaActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_segunda)


        val bundleRecepcion = intent.extras


        bundleRecepcion?.let {
            val nombre = it.getString("key_nombres", "") ?: ""
            val edad = it.getString("key_edad", "") ?: ""
            val mascota = it.getString("key_mascota", "") ?: ""
            val vacunas = it.getString("key_vacunas", "") ?: ""

            // MOSTRAR IMAGEN EN ESTA ACTIVITY
            if (mascota == getString(R.string.activity_main_perro )) {
                imgMascota.setImageResource(R.drawable.dog)
            } else if (mascota == getString(R.string.activity_main_gato )) {
                imgMascota.setImageResource(R.drawable.cat)
            } else if (mascota == getString(R.string.activity_main_conejo )) {
                imgMascota.setImageResource(R.drawable.rabbit)
            }

            tvResultadoNombre.text = ("El nombre de tu mascota es:   $nombre")
            tvEdad.text = ("La edad de tu mascota es:   $edad años")
            tvMascota.text = ("Tu mascota es un:  $mascota")
            tvVacunas.text = ("Vacunas: $vacunas")

        }

    }
}



