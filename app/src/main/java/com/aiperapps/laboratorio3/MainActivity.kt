package com.aiperapps.laboratorio3

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_segunda.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnEnviar.setOnClickListener {
            val nombres = edtNombres.text.toString()
            val edad = edtEdad.text.toString()

            if (nombres.isEmpty()) {
                Toast.makeText(this, "Debe ingresar sus nombres", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (edad.isEmpty()) {
                Toast.makeText(this, "Debe ingresar su edad", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            var mascota = ""
            if (rbPerro.isChecked) mascota += "Perro"
            if (rbGato.isChecked) mascota += "Gato"
            if (rbConejo.isChecked) mascota += "Conejo"

            var vacunas = " "
            if (!chkVacuna1.isChecked && !chkVacuna2.isChecked && !chkVacuna3.isChecked && !chkVacuna4.isChecked && !chkVacuna5.isChecked && !chkVacuna6.isChecked) {
                Toast.makeText(this, "Debe seleccionar al menos una vacuna", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (chkVacuna1.isChecked) vacunas += "Trivalente Felina"
            if (chkVacuna2.isChecked) vacunas += "Rabia"
            if (chkVacuna3.isChecked) vacunas += "Antirrabica"
            if (chkVacuna4.isChecked) vacunas += "Polivalente Canina"
            if (chkVacuna5.isChecked) vacunas += "Novarvilap"
            if (chkVacuna6.isChecked) vacunas += "Cunipravac"

         // GUARDAR BUNDLE
            val bundle = Bundle().apply {
                putString("key_nombres", nombres)
                putString("key_edad", edad)
                putString("key_mascota", mascota)
                putString("key_vacunas", vacunas)
            }

            // PASAR UNA ACTIVITY A OTRA
            val intent = Intent(this, SegundaActivity::class.java).apply {
                this.putExtras(bundle)
            }

            startActivity(intent)

        }

    }
}


